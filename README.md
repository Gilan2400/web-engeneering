<h2 style="text-align: center;">Übungshomepage zu "Einführung in Web Engineering Kaul SS2020" von Malte Karp</h1>

<a href="https://kaul.inf.h-brs.de/data/2019/we/index.html"><p style="text-align: center;">Link zur Veranstaltung</p></a>

---

Dieses Projekt ist eine Homepage, realisiert durch eine React Single Page Application. Sie zeigt die Übungen der Veranstaltung "Einführung in Web Engineering Kaul 2020", die von mir bearbeitet wurden. Ziel des Projekts war es das Hinzufügen von neuen Inhalten so einfach wie möglich zu gestalten, damit diese Webside semesterbegleitend benutzt werden kann, um Übungsergebnisse zu dokumentieren und zu präsentiren.  
                
---
Um die App local zu hosten ist es nötig, beim Start der App den gewünschten Port über die Umgebungsvariable REACT_APP_PORT anzugeben und die Umgebungsvariable REACT_APP_URL auf "localhost" zu setzen. 

```REACT_APP_PORT=3000 REACT_APP_URL=localhost npm start```


Um die App zu bauen muss das Build Script mit der URL der Zielumgebung gestartet werden.

```REACT_APP_URL=www2.inf.h-bonn-rhein-sieg.de/\~mkarp2s/ npm run-script build```

Zudem muss die URL der Zielumgebung unter "homepage" in der Package.json angegeben werden.

```"homepage":"http://www2.inf.h-bonn-rhein-sieg.de/~mkarp2s/"```


Für die App verwendeten Module sind: 

+ facebook/create-react-app
<a href="https://github.com/facebook/create-react-app">https://github.com/facebook/create-react-app</a>

+ react-router-dom
<a href="https://www.npmjs.com/package/react-router-dom">https://www.npmjs.com/package/react-router-dom</a> 

+ react-iframe 
<a href="https://www.npmjs.com/package/react-iframe">https://www.npmjs.com/package/react-iframe</a>
<br/>
<br/>

--- 

##### Eine weitere Übung kann über drei einfache Schritte der Webseite hinzugefügt werden.

1. In Public unter Content ein neues Verzeichnis für die neue Übung anlegen z.B. "Uebung3" mit Unterverzeichnissen für die einzelnen Aufgaben z.B. "Aufagbe1".

2. In das Aufgabenverzeichnis den Inhalt der Aufgabe ablegen. Aufgabenüberschrift, Aufgabenstellung und mögliche Textlösungen werden in einer .json Datei gespeichert. Dabei ist zu beachten ,dass unter "ueberschrift" ein String erwaretet wird und unter "aufgabe" und "loesung" jeweils ein Array mit Strings. Um Code der zur Üebung gehört zu zeigen, muss dieser in eine .txt Datei kopiert werden und ebenfalls im Aufgabenverzeichnis abgelegt werden.
Html Dokumente, die zu einer Übung gehören, können auch im Aufgabenverzeichnis abgelegt werden, um auf der Homepage angezeigt zu werden. 

Beispiel Aufgabentext.json

```
    {
        "ueberschrift": "Responsiv mit Flexbox Desktop-First ",
        "aufgabe": ["Implementieren Sie ausschließlich mit HTML und CSS folgendes responsive Webdesign:","Implementieren Sie dies mit Flexbox-Layout Desktop-First."],
        "loesung": [""]
    }
```

3. Im letzten Schritt muss die menue.json um die neue Aufgabe ergänzt werden. Jedes Übungsobjekt verfügt über einen Namen, eine ID und ein Array für die Aufgaben. Jedes Aufgabenobjekt verfügt ebenfalls über einen Namen, eine ID und die optionale Angabe von drei Pfaden. Unter "textPfad" steht der Pfad zur .json mit der Aufgabenstellung und einer möglichen Textlösung. Unter "framePafd" steht der Pfad zur .html die angezeigt werden soll. Unter "codePfad" steht der Pfad zur .txt die den Code zur Aufgabe enthält.

Beispiel Übungsobjekt in menue.json

```  {
      "name": "Uebung5",
      "id": 4,
      "aufgaben": [
        {
          "id": 5.1,
          "name": "Aufgabe5.1",
          "textPfad": "content/Uebung5/Aufgabe1/Aufgabe1Text.json",
          "framePfad": "content/Uebung5/Aufgabe1/Einkaufsliste.html",
          "codePfad": "content/Uebung5/Aufgabe1/Aufgabe1Code.txt"
        },
        {
          "id": 5.2,
          "name": "Aufgabe5.2",
          "textPfad": "content/Uebung5/Aufgabe2/Aufgabe2Text.json",
          "framePfad": "content/Uebung5/Aufgabe2/Rednerliste.html",
          "codePfad": "content/Uebung5/Aufgabe2/Aufgabe2Code.txt"
        }
      ]
    }
```
---
Zurzeit wird diese Projekt mit meinen Übungsergebnissen der Lehrveranstaltung "Einführung in Web Engineerien Kaul SS2020" auf www2.inf.h-bonn-rhein-sieg.de gehostet.

<a href="http://www2.inf.h-bonn-rhein-sieg.de/~mkarp2s/"><p style="text-align: center;">Link zur Uebungshomepage</p></a>


