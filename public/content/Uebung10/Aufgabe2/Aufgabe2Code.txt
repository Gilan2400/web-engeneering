<!DOCTYPE html>
<html>

<head>
    <title>Aufgabe1</title>
    <meta charset="utf-8" />
</head>

<body>
    <div id="app">
        <menue givenbutton=5 givenarray= '["test1","test2","test3","test4","test5"]' givenhorizontal = true></menue>
    </div>
</body>
<style>
   
</style>


<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script>

    Vue.component('menue', {
        template: `
        
            <button v-if="{hrozontal}" v-for="value in array" @click="clickHandler">value</button>
            <button v-else v-for="value in array" @click="clickHandler">value</button><br/>
           
            `
            ,
        props: ['givenbutton', 'givenarray', 'givenhorizontal'],

        data: function () {
            return {
                button: this.givenbutton,
                array: this.givenarray,
                horizontal: this.givenhorizontal
            }
        },
        computed: {
           
        },

        methods: {
            
        }

    });

    new Vue({
        el: '#app'
    });
</script>






</html>