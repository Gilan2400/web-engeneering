import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';

import NavigationMenue from './components/NavigationMenue';
import ContentMenueWrapper from './components/ContentMenueWrapper'
import Home from './components/Home'

class App extends Component {


  state = {
    uebungen: []
  }


  getMenue = () => {
    let url;
    if (process.env.REACT_APP_PORT != undefined) {
        url = "http://" + process.env.REACT_APP_URL + ":" + process.env.REACT_APP_PORT +"/Menue.json"

    } else if ( process.env.REACT_APP_URL !=  undefined && process.env.REACT_APP_PORT == undefined){
        url = "http://" + process.env.REACT_APP_URL + "Menue.json"
    } else {
        url = "/Menue.json";
    }
    console.log(url)

    fetch(url)
      .then(res => res.json())
      .then((data) => {
        this.setState({ uebungen: data.uebungen })
      })


  }

  componentDidMount() {
    this.getMenue();
  }

  render() {

    return (
      <Router>
        <div className="App" style={{ backgroundColor: "#D9E5F2", margin: "0px" }}>

          <header style={{ backgroundColor: "#2D4056" }}>
            <h1 style={{ textAlign: "center", padding: "2%", margin: "0px", color:"#D9E5F2"}}>Web Engineering Kaul SS2020</h1>
            <div style={{ display: "flex", justifyContent: "flex-start", flexWrap: "wrap"}}>
              <NavigationMenue uebungen={this.state.uebungen}></NavigationMenue>
            </div>
          </header>
          <div>
            <Route path = "/home" render={()=><Home></Home>}></Route>
            <ContentMenueWrapper uebungen={this.state.uebungen}></ContentMenueWrapper>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
