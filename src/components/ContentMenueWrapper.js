import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import TaskMenue from './TaskMenue';
import Content from './Content';



class ContentMenueWrapper extends Component {

    menuRuten = () => {

        return this.props.uebungen.map(uebung => (

            <Route key={uebung.id} path={"/" + uebung.name} render={() => <TaskMenue uebung={uebung} ></TaskMenue>} />


        ))
    }
    contentRuten = () => {
        const inhalt = this.props.uebungen.map(uebung => {
            return uebung.aufgaben.map(aufgabe => (
                <Route key={aufgabe.id} exact path={"/" + uebung.name + "/" + aufgabe.name} render={() => <Content aufgabe={aufgabe} uebung={uebung}></Content>}/>
            ))
        }
        )
        return (<div>{inhalt}</div>)
    }

    render() {
        //console.log(this.props.location?.pathname)
        return (
            <div style={{ display: "flex", flexDirection: "row" }}>

                <div style={{ display: "flex", flexDirection: "column", paddingTop: "10%", backgroundColor: "#2D4056" }}>
                    {this.menuRuten()}
                </div>
                <div style={{margin: "25px 25px 25px 25px", width: "100%" }} >
                   {this.contentRuten()}
                </div>
               

            </div>

        )
    }
}
export default ContentMenueWrapper;
