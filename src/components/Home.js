import React, { Component } from 'react';
import { Link } from 'react-router-dom';



class Home extends Component {


    render() {

        return (
            <div style={{ display: "flex", flexDirection: "row" }}>

                <div style={{ display: "flex", backgroundColor: "#2D4056", width: "2%", margin: "0px" }}></div>

                <div style={{ width: "96%", margin: "0px", height: "1000px" }}>
                    <h2 style={{ textAlign: "center" }}>Willkommen auf der Uebungshomepage zu "Einführung in Web Engineering Kaul SS2020" von Malte Karp</h2>
                    <div style={{ textAlign: "center",fontSize: "larger" }}>
                        <a href="https://kaul.inf.h-brs.de/data/2019/we/index.html" > Link zur Webside der Veranstaltung</a>
                    </div>
                    <div style={{ textAlign: "center", fontSize: "larger" }}>
                        <p>Diese Homepage ist eine React Single Page Application, die die Übungen der Veranstaltung "Einführung in Web Engineering Kaul SS2020" anzeigt, die von mir bearbeitet wurden.
                            <br />
                            Ziel des Projekts war es das Hinzufügen von neuen Inhalten so einfach wie möglich zu gestalten, damit diese Webside semesterbegleitend 
                            genutzt werden kann, um Übungsergebnisse zu dokumentieren und zu präsentieren.
                            <br />
                        </p>
                        <p>Wem die Webside gefällt und dran interessiert ist eine ähnliche Webside mit seinen eigenen Lösungen zu nutzen, ist gerne dazu eingeladen dieses Projekt von GitLab zu clonen.
                            <br />Dort befindet sich auch eine detaillierte Anleitung wie man die Webside hostet und Inhalte hinzufügt.
                        </p>

                    </div>
                    <div style={{textAlign: "center", fontSize: "larger"}}>
                        <a href="https://gitlab.com/Gilan2400/web-engeneering/"> Link zum GitLab Repository</a>
                    </div>
                </div>

                <div style={{ display: "flex", backgroundColor: "#2D4056", width: "2%", margin: "0px" }}></div>
            </div>
        )


    }
}
export default Home;
