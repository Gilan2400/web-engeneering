import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import menueButton from '../css/menueButton.css'


class NavigationMenue extends Component {


    render() {

        return this.props.uebungen.map(uebung => (
            <Link to={"/" + uebung.name}> <button class="menueButton">{uebung.name}</button> </Link>
        ))


    }
}
export default NavigationMenue;