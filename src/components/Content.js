import React, { Component } from 'react';
import Iframe from 'react-iframe'




class Content extends Component {
    state = {}

    getText = () => {
        let url;
        if (process.env.REACT_APP_PORT != undefined) {
            url = "http://" + process.env.REACT_APP_URL + ":" + process.env.REACT_APP_PORT + "/" + this.props.aufgabe.textPfad

        } else if (process.env.REACT_APP_URL != undefined && process.env.REACT_APP_PORT == undefined) {
            url = "http://" + process.env.REACT_APP_URL + this.props.aufgabe.textPfad
        } else {
            url = this.props.aufgabe.textPfad;
        }
        
        fetch(url)
            .then(response => response.json())
            .then((data) => {
                this.setState({ data: data })
            })

    }

    renderText = () => {
        return (

            <div>
                <h2 style={{ textAlign: "center" }}>{this.state.data?.ueberschrift}</h2>
                {this.state.data?.aufgabe.map(aufgabenZeile => (<p style={{ margin: "0px", fontSize: "larger" }}>{aufgabenZeile}</p>))}
                <br />
                {this.state.data?.loesung.map(loesnugZeile => (<p style={{ margin: "0px", fontSize: "larger" }}>{loesnugZeile}</p>))}
            </div>
        )

    }

    renderContentFrame = () => {
        if (this.props.aufgabe.framePfad != undefined && this.props.aufgabe.framePfad !== "") {
            let src;
            if (process.env.REACT_APP_PORT != undefined) {
                src = "http://" + process.env.REACT_APP_URL + ":" + process.env.REACT_APP_PORT + "/" + this.props.aufgabe.framePfad

            } else if (process.env.REACT_APP_URL != undefined && process.env.REACT_APP_PORT == undefined) {
                src = "http://" + process.env.REACT_APP_URL + this.props.aufgabe.framePfad
            } else {
                src = this.props.aufgabe.framePfad;
            }

            return <Iframe src={src} title="Content" width="100%" height="600px" scrolling={true} frameBorder="5"></Iframe>
        }
    }

    renderCodeFrame = () => {
        if (this.props.aufgabe.codePfad !== undefined && this.props.aufgabe.codePfad !== "") {
            let src;
            if (process.env.REACT_APP_PORT != undefined) {
                src = "http://" + process.env.REACT_APP_URL + ":" + process.env.REACT_APP_PORT + "/" + this.props.aufgabe.codePfad

            } else if (process.env.REACT_APP_URL != undefined && process.env.REACT_APP_PORT == undefined) {
                src = "http://" + process.env.REACT_APP_URL + this.props.aufgabe.codePfad
            } else {
                src = this.props.aufgabe.codePfad;
            }
            return <Iframe src={src} title="code" width="100%" height="600px"></Iframe>
        }
    }

    componentDidMount() {
        this.getText();
    }




    render() {
        return (
            <div >

                {this.renderText()}

                <div style={{ backgroundColor: "white" }}>
                    {this.renderContentFrame()}
                </div>
                <div style={{ backgroundColor: "white", marginTop: "2%" }}>
                    {this.renderCodeFrame()}
                </div>


            </div>
        );
    }
}

export default Content;
