import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import menueButton from '../css/menueButton.css'
class TaskMenue extends Component {

    render() {
        return (
            <div style={{ display: "flex", flexDirection: "column", justifyContent: "flex-start", flexWrap: "wrap", height: "800px", width: "100%" }}>
                {this.props.uebung.aufgaben.map((aufgabe) => (
                    <Link to={"/" + this.props.uebung.name + "/" + aufgabe.name}> <button class="menueButton">{aufgabe.name} </button></Link>
                ))}
            </div>

        )
    }
}

export default TaskMenue;